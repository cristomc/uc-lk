<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require  __DIR__ . '/../../vendor/autoload.php';

$app = AppFactory::create();
$app->addBodyParsingMiddleware();
/*
$app->get('/api/', function (Request $request, Response $response, $args) {
    $data = array('status' => 'OK');
    $payload = json_encode($data);

    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});
*/
$app->group('/api', function ($gr) {
    $gr->group('/v1', function ($group) {
        require  __DIR__ . '/../../src/Discounts/Discount.routes.php';
        require  __DIR__ . '/../../src/Orders/Order.routes.php';
        require  __DIR__ . '/../../src/Products/Product.routes.php';
        require  __DIR__ . '/../../src/Users/User.routes.php';
    });
});

$app->run();
