<html>

<head>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/milligram/1.3.0/milligram.css">
    <link rel="stylesheet" href="./assets/css/styles.css">

</head>

<body>
    <div id="root"></div>
    <!-- 
            Due it's just a demo, I use unpkg min libs to quick development.
            In a real-world project it should be an npm project inside for using
            react with all additional packages and babel to transpile properly and 
            webpack to make all tasks needed.
        -->

    <!-- DEV PKGs
    <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
    <script src='https://unpkg.com/react-router-dom@5.1.2/umd/react-router-dom.js'></script>
-->

    <script src="https://unpkg.com/react@16/umd/react.production.min.js" crossorigin></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js" crossorigin></script>
    <script src='https://unpkg.com/react-router@5.1.2/umd/react-router.min.js'></script>
    <script src='https://unpkg.com/react-router-dom@5.1.2/umd/react-router-dom.min.js'></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <script src="./js/components/table.js" type="text/babel"></script>
    <script src="./js/components/header.js" type="text/babel"></script>
    <script src="./js/components/discount.js" type="text/babel"></script>
    <script src="./js/components/order.js" type="text/babel"></script>
    <script src="./js/components/product.js" type="text/babel"></script>
    <script src="./js/components/productList.js" type="text/babel"></script>
    <script src="./js/domain/login/loginForm.js" type="text/babel"></script>
    <script src="./js/domain/login/login.js" type="text/babel"></script>
    <script src="./js/domain/admin/admin.js" type="text/babel"></script>
    <script src="./js/domain/home/home.js" type="text/babel"></script>
    <script src="./js/app.js" type="text/babel"></script>
    <script src="./js/index.js" type="text/babel"></script>
</body>

</html>