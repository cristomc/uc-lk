function Table (props) {

    // STATE DEFINITIONS
    const [data, setData] = React.useState(props.data);
    const [title, setTitle] = React.useState(props.title);
    const [uri, setUri] = React.useState(props.uri);
    const [create, setCreate] = React.useState(false);
    const [filter, setFilter] = React.useState(props.filter);
    const [filterSelected, setFilterSelected] = React.useState('ALL');
    const [viewCreate, setViewCreate] = React.useState(false);
    const [newElement, setNewElement] = React.useState({
        id: null
    });
    const [, updateState] = React.useState();
    const forceUpdate = React.useCallback(() => updateState({}), []);

    // RENDER BLOCKS
    let createRow;
    let rowEdit = [];
    let headContent = [];
    let bodyContent = [];
    let buttonCreate;
    let filterBlock;


    // REACT LIFECYCLE
    React.useEffect(() => {
        setData(props.data);
        setTitle(props.title);
        setUri(props.uri);
        setCreate(props.enableCreate);
        setFilter(props.filter);
    });

    // ACTION METHODS
    const filterData = (event) => {
        setFilterSelected(event.target.value)
    }

    const setField = (event) => {
        let element = newElement;
        element[event.target.id] = event.target.value;
        setNewElement(element);
    }

    const createNew = () => {
        axios.post(`/api/v1/${uri}`, newElement)
        .then((response) => {
            let dataNew = data;
            dataNew.body.push(Object.values(response.data));
            setData(dataNew);
            forceUpdate();
            setNewElement({});
            setViewCreate(false);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    const createField = (event) => {
        setViewCreate(true);
    }

    // RENDER  & DATA ACTIONS

    for (const [i, item] of data.head.entries()) {
        rowEdit.push(<td key={i}>
            {item === 'id'? '' :
                <input type="text" id={item}  onChange={setField}></input> 
            }
        </td>);
    }
    rowEdit.push(<td key={201}><button className="button button-clear" onClick={() => createNew()}> Add</button></td>);

    for (const [i, item] of data.head.entries()) {
        headContent.push(<th key={i}>{item}</th>);
    }
    headContent.push(<th key={100}>actions</th>);

    for (const [i, row] of data.body.entries()) {
        let rowContent = [];
        for (const [index, field] of row.entries()) {
            rowContent.push(<td key={index}>{field}</td>)
        };

        if(filterSelected === 'ALL' || row.findIndex(el => el === filterSelected ) !== -1) {
            rowContent.push(<td key={i + 100}>-</td>); // TODO: here should be a button that enables new input with current data.
            bodyContent.push(<tr key={i}>{rowContent}</tr>);
        }
    }
    
    // RENDER CONTROLS 
    
    if(viewCreate) {
        createRow = <tr key={200}>{rowEdit}</tr>;
    }

    if(viewCreate) {
        bodyContent.push(createRow);
    }

    if (create === true) {
        buttonCreate = <button className="float-right button button-clear" onClick={() => createField()}> Add new</button>;
    }

    if(filter) {
        filterBlock= <div className="row">
            <div className="column">
                <p>Filter by state:</p>
                <select className="filter" onChange={filterData}>
                    <option value="ALL">all</option>
                    <option value="pending">pending</option>
                    <option value="acepted">acepted</option>
                    <option value="rejected">rejected</option>
                </select>
            </div>
        </div>
    }

    return (
        <React.Fragment>
            <h2>
                {title}
                {buttonCreate}
            </h2>
            {filterBlock}
            <table>
                <thead>
                    <tr>
                        {headContent}
                    </tr>
                </thead>
                <tbody>
                    {bodyContent}
                </tbody>
            </table>
        </React.Fragment>
    );
}