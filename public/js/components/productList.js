function ProductList (props) {
    const [list, setList] = React.useState(props.products);

    React.useEffect(() => {
        setList(props.products);
    });

    const addItem = (childData) => {
        props.onAdd(childData);
    }

    const final = [];
    for (let  product of list) {
        final.push(<Product key={product.id} data={product} onSelect={addItem}></Product>);
    }

    return (
        <React.Fragment>
            <section className="row">
                <div className="column column-100">
                    {final}
                </div>
            </section>
        </React.Fragment>
    );
}