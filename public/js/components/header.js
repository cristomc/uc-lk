function Header (props) {
    const history = ReactRouterDOM.useHistory();
    const [title, setProduct] = React.useState(props.title);
    const [user, setUser] = React.useState('');

    React.useEffect(() => {
        setUser(props.user);
    });

    const logOut = () => {
        sessionStorage.clear();
        history.push('/login')
    }

    return (
        <React.Fragment>
            <header className="global-header">
                <div className = "row" >
                    <div className="column column-70">
                        <h1>{title}</h1>
                    </div>
                    <div className="column column-30">
                        <p className="text-right">
                            <small>
                                Wellcome back {user.name}.
                            </small>
                            <button className="button button-clear" onClick={ () => logOut()}> Log out</button>
                        </p>
                    </div>
                </div>
            </header>
        </React.Fragment>
    );
}