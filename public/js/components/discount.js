function Discount (props) {
    const [product, setProduct] = React.useState(props.data);
    const [code, setCode] = React.useState('');
    const [InvalidCode, setInvalid] = React.useState(false);

    const addToOrder = () => {
        axios.get(`/api/v1/discount?code=${code}`)
        .then((response) => {
            if(response.data === false) {
                setInvalid(true);
            } else {
                props.onAddCode(response.data.id)
            }
        })
        .catch(function (error) {
            console.error(error);
        });
    }

    const updateValue =(event) => {
        setCode(event.target.value);
    }

    return (
        <React.Fragment>
            <article className="card">
                <section className="card-body">
                    <fieldset>
                        <label htmlFor="code">Insert your code:</label>
                        <input type="text" placeholder="12345" id="code" onChange={updateValue}/>
                        <small>{InvalidCode === true? 'Invalid code, please use other' : ''}</small>
                        <button className="button-primary button-outline btn-block" onClick={addToOrder}>Get discount</button>
                    </fieldset>
                </section>
            </article>
        </React.Fragment>
    );
}