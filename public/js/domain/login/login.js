class Login extends React.Component {
    render() {
        return (
        <div className="row">
            <div className="column column-50 column-offset-25">
                <h1>Login</h1>

                <LoginForm></LoginForm>
                <p>
                    List of users for the demo:
                </p>
                <ul>
                    <li>user@site.com / user</li>
                    <li>admin@site.com / admin</li>
                </ul>
            </div>
        </div>
        );
    }
}