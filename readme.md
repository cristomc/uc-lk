#Management system Use case

This repo contains the Use case for Lifekeys.
_Autor: Cristopher Caamana Gomez_

**Disclaimer:**

Due my current work I couldn't reach all points given in the use case. This is the checklist of the tasks done:

- [x] User is able to view products
- [x] User is able to place an order
- [x] User can use a discount code
- [x] Admin can Create/Read products
- [x] Admin can Create/Read discounts
- [x] Admin is able to view orders
- [x] Admin can view orders with specific status
- [ ] Admin can Update/Delete products/discounts (There is API,  but not FE)
- [ ] Admin can change order status (There is API, but not FE)


##Setup

This is a small project using composer an tested under PHP-cli environment on Ubuntu 20.04. For a quick setup there it will need:

- PHP 7+
- Composer
- SQLite module

###Setup commands

- clone this repo
- open a terminal on repo directory and run:
~~~~
    composer install
~~~~
- Give autostart exec permisions
~~~~
    chmod +x autostart.sh
~~~~

- Run with PHP-cli
~~~~
    ./autostart.sh
~~~~

##Project Structure

This demo project contains all stack prepared for quickstart. the directory structure is as follows:

- **config/**: Contains DB class to load SQLite database
- **public/**: Front-end project & root folder for www.
    -  **index.php**: Root file for serve to clients. It includes CDN elements for react (I prefeer use a npm project, but due timings I used this quick setup)
    -  **api/index.php**: Entrypoint for API calls.
    - **js/**: all React components are inside of the folder.
- **src/**: PHP source-code split in modules for each type of elements
- **vendor/**: composer-stuff elements.