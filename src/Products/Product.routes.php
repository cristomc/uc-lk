<?php

use App\Products\ProductController as Controller;
use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};

$controller = new Controller();

$group->get('/product[/{id}]', function (Request $request, Response $response, $args) use ($controller) {
    if($args['id']) {
        $data = $controller->getOne($args['id']);
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        $data = $controller->getMany();
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
});

$group->post('/product', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $createdProduct = $controller->create($body);
    if ($createdProduct->hasId()) {
        $payload = json_encode($createdProduct);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
        return $response->withStatus(401);
    }
});

$group->put('/product/{id}', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $updatedProduct = $controller->edit($args['id'], $body);
    if ($updatedProduct->hasId()) {
        $payload = json_encode($updatedProduct);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(202);
    } else {
        return $response->withStatus(401);
    }
});

$group->delete('/product/{id}', function (Request $request, Response $response, $args) use ($controller) {
    $userData = $controller->delete($args['id']);
    if ($userData === true) {
        $payload = json_encode($userData);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        return $response->withStatus(401);
    }
});
