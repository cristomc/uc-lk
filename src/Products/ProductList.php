<?php
namespace App\Products;

use Conf\BD as BBDD;

class ProductList {
    private $_bd;

    public $collection = Array();

    public function __construct() {
        $this->_bd = new BBDD();
    }

    public function getAll() {
        $res = $this->_bd->query("SELECT * FROM products");
        if ($res === false) {
            return $this;
        } else {
            while ($row = $res->fetchArray()) {
                array_push($this->collection,new Product($row));
            }
            return $this->collection;
        }
        $this->_db->close();
    }
}
