<?php


namespace App\Users;
use Conf\BD as BBDD;

class User {
    private $_bd;
    
    private $_id = NULL;
    public $name = NULL;
    public $email = NULL;
    public $password = NULL;
    public $type = NULL;
    
    public function __construct( $opt = ['name' => '', 'email' => '', 'password' => '', 'type' => ''] ){
        $this->name = isset($opt['name']) ? $opt['name'] : 'visitor';
        $this->email = isset($opt['email']) ? $opt['email'] : NULL;
        $this->password = isset($opt['password']) ? $opt['password'] : NULL;
        $this->type = isset($opt['type']) ? $opt['type'] : 'user';
        $this->_bd = new BBDD();
    }

    public function getUser() {
        $res = $this->_bd->querySingle("SELECT * FROM users where email =  '$this->email' AND password = '$this->password'", true);
        if($res === false) {
            return $this;
        } else {
            $this->_id = $res['id'];
            $this->id = $res['id'];
            $this->name = $res['name'];
            $this->type = $res['type'];
            unset($this->password);
            return $this;
        }
        $this->_db->close();
    }

    public function hasId() {
        return $this->_id !== null;
    }

}

?>