<?php

use App\Users\UserController as Controller;
use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};

$user = new Controller();

$group->get('/user/{id}', function (Request $request, Response $response, $args) {
    $data = array('status' => 'userOK');
    $payload = json_encode($data);
    $response->getBody()->write($payload);
    return $response->withHeader('Content-Type', 'application/json');
});

$group->post('/login', function (Request $request, Response $response, $args) use ($user) {
    $body = $request->getparsedBody();
    $userData = $user->login($body['email'],$body['password']);
    if ($userData->hasId()) {
        $payload = json_encode($userData);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        return $response->withStatus(401);
    }
});

?>