<?php

use App\Orders\OrderController as Controller;
use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};

$controller = new Controller();

$group->get('/order[/{id}]', function (Request $request, Response $response, $args) use ($controller) {
    if($args['id']) {
        $data = $controller->getOne($args['id']);
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        $data = $controller->getMany();
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    }
});

$group->post('/order', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $createdOrder = $controller->create($body);
    if ($createdOrder->hasId()) {
        $payload = json_encode($createdOrder);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
        return $response->withStatus(401);
    }
});

$group->put('/order/{id}', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $updatedProduct = $controller->edit($args['id'], $body);
    if ($updatedProduct->hasId()) {
        $payload = json_encode($updatedProduct);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(202);
    } else {
        return $response->withStatus(401);
    }
});