<?php
namespace App\Orders;

use Conf\BD as BBDD;

class OrderList {
    private $_bd;

    public $collection = Array();

    public function __construct() {
        $this->_bd = new BBDD();
    }

    public function getAll() {
        $res = $this->_bd->query("SELECT * FROM orders");
        if ($res === false) {
            return $this;
        } else {
            while ($row = $res->fetchArray()) {
                array_push($this->collection,new Order($row));
            }
            return $this->collection;
        }
        $this->_db->close();
    }
}
