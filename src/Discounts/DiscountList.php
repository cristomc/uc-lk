<?php
namespace App\Discounts;

use Conf\BD as BBDD;

class DiscountList {
    private $_bd;

    public $collection = Array();

    public function __construct() {
        $this->_bd = new BBDD();
    }

    public function getAll() {
        $res = $this->_bd->query("SELECT * FROM discounts");
        if ($res === false) {
            return $this;
        } else {
            while ($row = $res->fetchArray()) {
                array_push($this->collection,new Discount($row));
            }
            return $this->collection;
        }
        $this->_db->close();
    }
}
