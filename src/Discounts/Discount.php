<?php
namespace App\Discounts;

use Conf\BD as BBDD;

class Discount {

    private $_id = null;
    public $reference = null;
    public $code = null;
    public $used = null;


    public function __construct($opt = []) {
        $this->_id = isset($opt['id']) ? $opt['id'] : null;
        $this->reference = isset($opt['reference']) ? $opt['reference'] : null;
        $this->code = isset($opt['code']) ? $opt['code'] : NULL;
        $this->used = isset($opt['used']) ? $opt['used'] : NULL;
    }

    public function fetch() {
        $db = new BBDD();
        $res = $db->querySingle("SELECT * FROM discounts where id =  '$this->_id'", true);
        if ($res === false) {
            return $this;
        } else {
            $this->id = $res['id'];
            $this->reference = $res['reference'];
            $this->code = $res['code'];
            $this->used = $res['used'];
            return $this;
        }
        $this->_db->close();
    }

    public function filterBy($code) {
        $db = new BBDD();
        $res = $db->querySingle("SELECT id FROM discounts where code =  '$code' and used < 1", true);
        if ($res === false || sizeof($res) === 0) {
            return false;
        } else {
            return ['id' => $res['id']];
        }
        $this->_db->close();
    }

    public function upsert($args = null) {
        $db = new BBDD();
        if ($this->hasId()) {
            $this->fetch();
        }
        if ($args) {
            $this->reference = isset($args['reference']) ? $args['reference'] : $this->reference;
            $this->code = isset($args['code']) ? $args['code'] : $this->code;
            $this->used = isset($args['used']) ? $args['used'] : $this->used;
            $updateElements = "reference = '$this->reference', code = '$this->code', used = '$this->used'";
            $res = $db->exec("UPDATE discounts SET $updateElements WHERE id =  '$this->_id'");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                return $this;
            }
        } else {
            $elements = "(null,'$this->reference','$this->code','$this->used')";
            $res = $db->exec("INSERT INTO discounts VALUES $elements");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                $this->_id = $db->lastInsertRowID();
                $this->id = $this->_id;
                return $this;
            }
        }
    }

    public function delete() {
        $db = new BBDD();
        $res = $db->exec("DELETE FROM discounts where id =  '$this->_id'");
        if (!$res) {
            echo $db->lastErrorMsg();
            return false;
        } else {
            return true;
        }
        $this->_db->close();
    }

    public function hasId() {
        return $this->_id !== null;
    }
}

// TODO: estoy en la fase de implementar el resto de partes de la api. faltaria meterlos y empezar el front