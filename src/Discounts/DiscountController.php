<?php
namespace App\Discounts;

class DiscountController {

    public function __construct()
    {
    }


    public function getOne($id)
    {
        $discount = new Discount(['id' => $id]);
        return $discount->fetch();
    }

    public function getMany()
    {
        $list = new DiscountList();
        return $list->getAll();
    }

    public function findByCode($code)
    {
        $discount = new Discount();
        return $discount->filterBy($code);
    }

    public function create($args)
    {
        $discount = new Discount($args);
        return $discount->upsert();
    }

    public function edit($id, $args)
    {
        $discount = new Discount(['id' => $id]);
        return $discount->upsert($args);
    }

    public function delete($id)
    {
        $discount = new Discount(['id' => $id]);
        return $discount->delete();
    }
}


?>